import { productsFeatureName, ProductState } from '@app/modules/products-container/store/products.state';

export type ApplicationState = Readonly<{
  [productsFeatureName]: ProductState,
}>;
