import { Injectable, ErrorHandler} from '@angular/core'
import * as Sentry from '@sentry/browser'
import { environment as ENV } from '@environment';

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  
  constructor() {
    Sentry.init({ dsn: ENV.SENTRY, });
  }
  
  handleError(error): void {
    Sentry.captureException(error.originalError || error)
  }

}
