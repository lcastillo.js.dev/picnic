import { Component, OnInit } from '@angular/core';
import { ON_BOARDING_IMAGES } from '../../constants/on-boarding-images';

@Component({
  selector: 'app-on-boarding',
  templateUrl: './on-boarding.component.html',
  styleUrls: ['./on-boarding.component.sass']
})
export class OnBoardingComponent implements OnInit {

  public onBoardingImages: { image: string, name: string }[] = ON_BOARDING_IMAGES;

  constructor() { }

  ngOnInit(): void {
  }

}
