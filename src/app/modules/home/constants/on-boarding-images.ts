export const ON_BOARDING_IMAGES = [
  { image: 'https://www.picnic.app/static/usp1-2b9f14929a17c6bc748a579332623843.svg', name: 'Jij bestelt, wij doen de rest' },
  { image: 'https://www.picnic.app/static/usp2-660ad216f665151b36ac861f1313d7bb.svg', name: 'Altijd gratis thuisbezorgd' },
  { image: 'https://www.picnic.app/static/usp3-8008d23f5bbc90fd9d1644d15ef9b935.svg', name: 'Bespaar tijd en gesjouw' },
];
