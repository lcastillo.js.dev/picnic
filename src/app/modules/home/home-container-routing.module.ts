import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomeContainer } from './home.component';

export const homeRootRoute = '';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: homeRootRoute,
        component: HomeContainer
      }
    ])
  ],
  exports: [ RouterModule ]
})
export class HomeContainerRoutingModule {}
