import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeContainer } from './home.component';
import { OnBoardingComponent } from './components/on-boarding/on-boarding.component';
import { CarouselModule } from '../core/carousel/carousel.module';
import { HomeContainerRoutingModule } from './home-container-routing.module';

@NgModule({
  declarations: [
    HomeContainer,
    OnBoardingComponent
  ],
  imports: [
    CommonModule,
    CarouselModule,
    HomeContainerRoutingModule
  ]
})
export class HomeModule { }
