import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ShoppingCartComponent } from './shopping-cart.component';

const shoppingCartRootRoute = 'checkout';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: shoppingCartRootRoute,
        component: ShoppingCartComponent
      },
    ])
  ],
  exports: [ RouterModule ]
})
export class ShoppingCartContainerRouteModule {}
