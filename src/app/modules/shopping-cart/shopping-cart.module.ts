import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShoppingCartContainerRouteModule } from './shopping-cart-routing.module';
import { ShoppingCartComponent } from './shopping-cart.component';
import { StepperComponent } from './components/stepper/stepper.component';
import { ReviewComponent } from './components/review/review.component';
import { ShoppingCartFacade } from './shopping-cart.facade';
import { CreditCardComponent } from './components/credit-card/credit-card.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ShoppingCartComponent,
    StepperComponent,
    ReviewComponent,
    CreditCardComponent,
  ],
  imports: [
    CommonModule,
    ShoppingCartContainerRouteModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    ShoppingCartFacade
  ]
})
export class ShoppingCartModule { }
