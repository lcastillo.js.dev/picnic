import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, interval, Subscription } from 'rxjs';

import { ShoppingCartFacade } from './shopping-cart.facade';
import { Product } from '../products-container/entities/product.entities';
import { SHOPPING_CART_SECTIONS } from './constants/sh-sections.constants';
import { Router } from '@angular/router';

const PAYMENT_MOCK_SEC = 3000;

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.sass']
})
export class ShoppingCartComponent implements OnInit, OnDestroy {

  public sections = SHOPPING_CART_SECTIONS;
  public selectedSection: string = SHOPPING_CART_SECTIONS.REVIEW;
  public isPaymentProcess: boolean;
  private subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private facade: ShoppingCartFacade
  ) { }

  ngOnInit(): void { }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  get shoppingCartProducts$(): Observable<Product[]> {
    return this.facade.shoppingCartProducts$;
  }

  public goToCheckout(): void {
    if (this.selectedSection === SHOPPING_CART_SECTIONS.CHECKOUT) {
      this.isPaymentProcess = true;
      this.subscriptions.push(
        interval(PAYMENT_MOCK_SEC).subscribe(() => (
          this.isPaymentProcess = false, this.router.navigate(['/list']
        ))),  
      );
    } else {
      this.selectedSection = SHOPPING_CART_SECTIONS.CHECKOUT;
    }
  }

}
