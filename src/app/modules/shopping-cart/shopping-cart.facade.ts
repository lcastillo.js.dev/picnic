import { Injectable } from "@angular/core";
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApplicationState } from '@app/store/state';
import { Product } from '../products-container/entities/product.entities';
@Injectable()
export class ShoppingCartFacade {

  constructor(private store: Store<ApplicationState>) { }

  public shoppingCartProducts$: Observable<Product[]> = this.store.pipe(
    map(state => state.productsModule.shoppingCart),
  );

}
