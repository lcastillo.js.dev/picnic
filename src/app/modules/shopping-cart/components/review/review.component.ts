import { Component, OnInit, Input } from '@angular/core';

import { Product } from '@app/modules/products-container/entities/product.entities';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.sass']
})
export class ReviewComponent implements OnInit {

  @Input() products: Product[];

  constructor() { }

  ngOnInit(): void { }

  get totalPrice(): number {
    return this.products.map(
      ({ price }) => typeof price === 'string' ? parseInt(price) : price
    ).reduce((acc, cur) => acc + cur, 0);
  }

}
