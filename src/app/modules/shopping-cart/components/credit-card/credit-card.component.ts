import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.sass']
})
export class CreditCardComponent implements OnInit {

  public form: FormGroup = {} as FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.form = new FormGroup({
      ccNumber: new FormControl('0000 0000 0000 0000', [Validators.required, Validators.max(16)]),
      expMonth: new FormControl('00', [Validators.required]),
      expYear: new FormControl('00', [Validators.required]),
      cvvNumber: new FormControl('000', [Validators.required])
    })
  }

  public onCreditCardChange(key: string): void {
    this.form.patchValue({ ccNumber: '' });
  }

}
