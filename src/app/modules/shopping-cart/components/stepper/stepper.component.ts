import { Component, OnInit, Input } from '@angular/core';
import { SHOPPING_CART_SECTIONS } from '../../constants/sh-sections.constants';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.sass']
})
export class StepperComponent {

  @Input() selectedSection: string;

  public sections = SHOPPING_CART_SECTIONS;

}
