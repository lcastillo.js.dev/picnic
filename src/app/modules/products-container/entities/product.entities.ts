export interface ProductListResponse {
  products: Product[];
}

export interface Product {
  product_id: string;
  name: string;
  price: number | string;
  image: string;
  description?: string;
}
