import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { ProductsServices } from '../../products-container.service';
import { productsActions } from '../actions/products.actions';
import { switchMap, map } from 'rxjs/operators';

@Injectable()
export class ProductsEffects {

  constructor(
    private actions$: Actions,
    private service: ProductsServices
  ) { }

  fetchProductsList$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(productsActions.fetchProductsListAction),
      switchMap(() => this.service.fetchProductsList().pipe(
        map(response => productsActions.fetchProductsListSuccessAction({ response} ))
      ))
    )
  );

  fetchProductById$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(productsActions.fetchProductByIdAction),
      switchMap((action) => this.service.fetchProductById(action.payload).pipe(
        map(response => productsActions.fetchProductByIdSuccessAction({ response }))
      ))
    )
  );

}
