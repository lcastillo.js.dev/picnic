import { on, createReducer, Action } from '@ngrx/store';

import produce from 'immer';
import { productsActions } from '../../actions/products.actions';

export const initialState = false;

export const reducer = createReducer(
  initialState,
  on(productsActions.fetchProductsListAction, () => true),
  on(productsActions.fetchProductsListSuccessAction, () => false),
  on(productsActions.fetchProductByIdAction, () => true),
  on(productsActions.fetchProductByIdSuccessAction, () => false)
);

export const loadingProductReducer = produce((state: boolean, action: Action) => {
  return reducer(state, action);
});
