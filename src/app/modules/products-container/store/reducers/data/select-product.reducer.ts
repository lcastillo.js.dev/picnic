import { on, createReducer, Action } from '@ngrx/store';

import produce from 'immer';
import { Product } from '../../../entities/product.entities';
import { productsActions } from '../../actions/products.actions';

export const initialState = {} as Product;

export const reducer = createReducer(
  initialState,
  on(productsActions.fetchProductByIdSuccessAction, (state, { response }) => {
    return { ...response };
  })
);

export const selectedProductReducer = produce((state: Product, action: Action) => {
  return reducer(state, action);
});
