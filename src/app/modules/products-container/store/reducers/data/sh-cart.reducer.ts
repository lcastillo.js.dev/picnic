import { on, createReducer, Action } from '@ngrx/store';

import produce from 'immer';
import { Product } from '../../../entities/product.entities';
import { shoppingCartActions } from '../../actions/sh-cart.actions';

export const initialState: Product[] = [];

export const reducer = createReducer(
  initialState,
  on(shoppingCartActions.addProductToCartAction, (state, { payload }) => {
    return [ ...state, payload ];
  }),
)

export const shoppingCartListReducer = produce((state: Product[], action: Action) => {
  return reducer(state, action);
});
