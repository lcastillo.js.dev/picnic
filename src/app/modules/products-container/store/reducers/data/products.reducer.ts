import { on, createReducer, Action } from '@ngrx/store';

import produce from 'immer';
import { Product } from '../../../entities/product.entities';
import { productsActions } from '../../actions/products.actions';

export const initialState = [];

export const reducer = createReducer(
  initialState,
  on(productsActions.fetchProductsListSuccessAction, (state, { response }) => {
    return [ ...response.products ];
  }),
)

export const productsReducer = produce((state: Product[], action: Action) => {
  return reducer(state, action);
})
