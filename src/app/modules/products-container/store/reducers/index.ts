import { combineReducers } from '@ngrx/store';

import { productsReducer as products } from './data/products.reducer';
import { loadingProductReducer as loading } from './ui/loading.reducer';
import { selectedProductReducer as selectedProduct } from './data/select-product.reducer';
import { shoppingCartListReducer as shoppingCart } from './data/sh-cart.reducer';

export const productsRootReducer = combineReducers({
  products,
  selectedProduct,
  shoppingCart,
  loading,
});
