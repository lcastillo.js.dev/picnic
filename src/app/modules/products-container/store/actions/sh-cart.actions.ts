import { createAction, props } from '@ngrx/store';
import { type } from '@app/util/util';
import { Product } from '../../entities/product.entities';

export const addProductToCartAction = createAction(
  type('[Sh-Cart] Add product to cart'),
  props<{ payload: Product }>()
);

export const shoppingCartActions = {
  addProductToCartAction,
};
