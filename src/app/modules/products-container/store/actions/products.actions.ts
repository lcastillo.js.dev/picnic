import { createAction, props } from '@ngrx/store';
import { type } from '@app/util/util';
import { ProductListResponse, Product } from '../../entities/product.entities';

export const fetchProductsListAction = createAction(
  type('[Products] Get Products List')
);
export const fetchProductsListSuccessAction = createAction(
  type('[Products] Get Products List Success'),
  props<{ response: ProductListResponse }>()
);
export const fetchProductByIdAction = createAction(
  type('[Products] Get Products By Id'),
  props<{ payload: string }>()
);
export const fetchProductByIdSuccessAction = createAction(
  type('[Products] Get Products By Id Success'),
  props<{ response: Product }>()
);

export const productsActions = {
  fetchProductsListAction,
  fetchProductsListSuccessAction,
  fetchProductByIdAction,
  fetchProductByIdSuccessAction,
};
