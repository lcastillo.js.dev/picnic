import { Product } from '../entities/product.entities';

export const productsFeatureName = 'productsModule';

export type ProductState = Readonly<{
    products: Product[];
    selectedProduct: Product;
    shoppingCart: Product[],
    loading: boolean;
}>;
