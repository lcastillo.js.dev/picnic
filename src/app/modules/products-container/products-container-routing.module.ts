import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProductsContainer } from './products.container';

export const productsRootRoute = 'list';
export const productIdRootRoute = 'list/:productId';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: productsRootRoute,
        component: ProductsContainer
      },
      {
        path: productIdRootRoute,
        component: ProductsContainer
      }
    ])
  ],
  exports: [ RouterModule ]
})
export class ProductsContainerRoutingModule {}
