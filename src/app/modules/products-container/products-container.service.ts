import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ProductListResponse, Product } from './entities/product.entities';
import { environment as ENV } from '@environment';

@Injectable()
export class ProductsServices {

  constructor(private httpClient: HttpClient) { }

  public fetchProductsList(): Observable<ProductListResponse> {
    return this.httpClient.get<ProductListResponse>(`${ENV.api.cors}${ENV.api.baseUrl}/cart/list`);
  }

  public fetchProductById(id: string): Observable<Product> {
    const endpoint = `${ENV.api.cors}${ENV.api.baseUrl}/cart/${id}/detail`;
    return this.httpClient.get<Product>(endpoint);
  }

}