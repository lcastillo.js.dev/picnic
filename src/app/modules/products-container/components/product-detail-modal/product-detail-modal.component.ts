import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Product } from '../../entities/product.entities';
import { MODAL_SIZES } from '@app/modules/core/modals/constants/modal-size.contants';
import { imageBackgroundContainer } from '../../utils/image-bg';
import { GeneralEntity } from '@app/modules/core/entities/general';

@Component({
  selector: 'app-product-detail-modal',
  templateUrl: './product-detail-modal.component.html',
  styleUrls: ['./product-detail-modal.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductDetailModalComponent implements OnInit {

  @Input() isOpen: boolean;
  @Input() product: Product;
  @Input() isLoading: boolean;
  @Input() isMobile: boolean;
  
  public modalSize: string = MODAL_SIZES.XLARGE;
  @Output() onClose: EventEmitter<void> = new EventEmitter<void>();
  @Output() onAddToCartChange: EventEmitter<Product> = new EventEmitter<Product>();

  constructor() { }

  ngOnInit(): void { }

  get imageBackground(): GeneralEntity {
    return imageBackgroundContainer('395px', '400px', this.product.image);
  }

  get isNullProduct(): boolean {
    return !this.product || Object.entries(this.product).length === 0;
  }

  public onAddToCart(): void {
    this.onAddToCartChange.emit(this.product);
  }

  public onCloseHandler(): void {
    this.onClose.emit()
  }

}
