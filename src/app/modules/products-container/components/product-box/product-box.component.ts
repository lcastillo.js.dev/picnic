import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Product } from '../../entities/product.entities';
import { imageBackgroundContainer } from '../../utils/image-bg';
import { GeneralEntity } from '@app/modules/core/entities/general';

@Component({
  selector: 'app-product-box',
  templateUrl: './product-box.component.html',
  styleUrls: ['./product-box.component.sass']
})
export class ProductBoxComponent implements OnInit {

  @Input() product: Product;
  @Input() isLoading: boolean;

  @Output() onProductBoxClick: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void { }

  get imageBackground(): GeneralEntity {
    return imageBackgroundContainer('180px', '150px', this.product.image);
  }

  public onProductBoxClickHandler() {
    this.onProductBoxClick.emit(this.product.product_id);
  }

}
