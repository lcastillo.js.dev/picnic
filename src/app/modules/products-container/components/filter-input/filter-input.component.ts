import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-filter-input',
  templateUrl: './filter-input.component.html',
  styleUrls: ['./filter-input.component.sass']
})
export class FilterInputComponent implements OnInit {

  @Input() disabled: boolean;

  public keyFilteredWord: string = '';
  @Output() onInputFilterChange: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  public onInputChanges(key: string): void {
    this.onInputFilterChange.emit(key);
  }

}
