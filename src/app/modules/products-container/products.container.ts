import { Component, OnInit, Inject, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription, combineLatest } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { ProductsFacade } from './products-container.facade';
import { Product } from './entities/product.entities';
import { IS_MOBILE } from '@app/constants/app.tokens';
import { Location } from '@angular/common';
import { ROUTES } from './constants/routes';

@Component({
  selector: 'app-products',
  templateUrl: './products.container.html',
  styleUrls: ['./products.container.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductsContainer implements OnInit, OnDestroy {

  public isProductModalOpen: boolean = false;
  public keyToFilter: string = '';
  public productsSkeletonsArray: number[] = Array(12).fill(6);
  private subscriptions: Subscription[] = [];

  constructor(@Inject(IS_MOBILE) public isMobile: boolean,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private location: Location,
              private facade: ProductsFacade,
  ) { }

  ngOnInit(): void {
    this.facade.fetchProducts();
    this.routeEvents();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  get isLoading$(): Observable<boolean> {
    return this.facade.isLoading$;
  }

  get products$(): Observable<Product[]> {
    return this.facade.products$.pipe(map(products => 
      products.filter(product => 
        product.name.toLowerCase().includes(this.keyToFilter.toLowerCase())
      )
    ));
  }

  get selectedProduct$(): Observable<Product> {
    return this.facade.selectedProduct$;
  }

  public routeEvents(): void {
    this.subscriptions.push(
      this.activatedRoute.params.pipe(
        filter(params => params.productId)
      ).subscribe(params => this.fetchProductById(params.productId)),
    );
  }

  get showSkeletons$(): Observable<boolean> {
    return combineLatest(this.isLoading$, this.products$).pipe(
      map(([ isLoading, products ]) => isLoading && products.length === 0),
    );
  }

  public onAddToCartChange(product: Product): void {
    this.facade.addProductToCart(product);
  }
  
  public onInputFilterChange(key: string): void {
    this.keyToFilter = key;
  }

  public onCloseDetailModal(): void {
    this.isProductModalOpen = false;
    this.location.replaceState(ROUTES.LIST);
  }

  public onSelectProductById(productId: string): void {
    this.router.navigate([ROUTES.LIST, productId]);
  }

  public fetchProductById(productId: string): void {
    this.isProductModalOpen = true;
    this.facade.fetchProductById(productId);
  }

}
