import { NgModule, InjectionToken } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { ActionReducerMap, StoreModule } from '@ngrx/store';

import { ProductsContainer } from './products.container';
import { ProductsContainerRoutingModule } from './products-container-routing.module';
import { ProductsFacade } from './products-container.facade';
import { ProductState, productsFeatureName } from './store/products.state';
import { ProductsEffects } from './store/effects/products.effects';
import { ProductsServices } from './products-container.service';
import { ProductBoxComponent } from './components/product-box/product-box.component';
import { ModalModule } from '@app/modules/core/modals/modal.module';
import { ProductDetailModalComponent } from './components/product-detail-modal/product-detail-modal.component';
import { productsRootReducer } from './store/reducers';
import { CarouselModule } from '../core/carousel/carousel.module';
import { FilterInputComponent } from './components/filter-input/filter-input.component';

export const PRODUCTS_REDUCER_FEATURE_TOKEN = new InjectionToken<
  ActionReducerMap<ProductState>
>('Feature Product Reducers');

@NgModule({
  declarations: [
    ProductsContainer,
    ProductBoxComponent,
    ProductDetailModalComponent,
    FilterInputComponent
  ],
  imports: [
    CommonModule,
    ProductsContainerRoutingModule,
    FormsModule,
    StoreModule.forFeature(productsFeatureName, PRODUCTS_REDUCER_FEATURE_TOKEN),
    EffectsModule.forFeature([ProductsEffects]),
    ModalModule,
    CarouselModule,
  ],
  providers: [
    ProductsServices,
    ProductsFacade,
    {
      provide: PRODUCTS_REDUCER_FEATURE_TOKEN,
      useValue: productsRootReducer
    }
  ]
})
export class ProductsContainerModule { }
