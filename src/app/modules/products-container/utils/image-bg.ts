import { GeneralEntity } from '@app/modules/core/entities/general';

export function imageBackgroundContainer(width: string, height: string, image: string): GeneralEntity {
  return {
    width,
    height,
    backgroundImage: `url(${image})`,
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  }
}
