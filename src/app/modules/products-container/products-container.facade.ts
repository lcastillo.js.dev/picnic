import { Injectable } from "@angular/core";
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { ApplicationState } from '@app/store/state';
import { Product } from './entities/product.entities';
import { productsActions } from './store/actions/products.actions';
import { shoppingCartActions } from './store/actions/sh-cart.actions';

@Injectable()
export class ProductsFacade {

  constructor(private store: Store<ApplicationState>) { }

  public products$: Observable<Product[]> = this.store.pipe(
    map(state => state.productsModule.products),
  );
  public selectedProduct$: Observable<Product> = this.store.pipe(
    map(state => state.productsModule.selectedProduct),
  );
  public shoppingCart$: Observable<Product[]> = this.store.pipe(
    filter(state => !!state.productsModule),
    map(state => state.productsModule.shoppingCart || [])
  );
  public isLoading$: Observable<boolean> = this.store.pipe(
    map(state => state.productsModule.loading),
  );

  public fetchProducts(): void {
    this.store.dispatch(productsActions.fetchProductsListAction());
  }

  public fetchProductById(productId: string): void {
    this.store.dispatch(productsActions.fetchProductByIdAction({ payload: productId }));
  }

  public addProductToCart(product: Product): void {
    this.store.dispatch(shoppingCartActions.addProductToCartAction({ payload: product }));
  }

}
