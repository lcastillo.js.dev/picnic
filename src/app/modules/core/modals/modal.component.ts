import {
  Component, Output, EventEmitter, Input, ChangeDetectionStrategy
} from '@angular/core';

import { MODAL_SIZES } from './constants/modal-size.contants';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalComponent {

  @Input() isOpen: boolean = true;
  @Input() isMobile: boolean = false;
  @Input() modalSize: string = MODAL_SIZES.LARGE;
  @Input() showCloseButton: boolean = true;

  @Output() onClose: EventEmitter<Event> = new EventEmitter<Event>();

  constructor() { }

  public dispatchCloseModal(): void {
    this.onClose.emit();
  }

}
