export enum MODAL_SIZES {
  SMALL = '440px',
  MEDIUM = '530px',
  LARGE = '570px',
  XLARGE = '750px',
}
