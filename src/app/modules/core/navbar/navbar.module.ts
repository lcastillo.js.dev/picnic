import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NavbarComponent } from './navbar.component';
import { ProductsFacade } from '@app/modules/products-container/products-container.facade';

@NgModule({
  declarations: [
    NavbarComponent,
  ],
  imports: [
    BrowserModule,
  ],
  providers: [
    ProductsFacade
  ],
  exports: [
    NavbarComponent
  ]
})
export class NavbarModule { }
