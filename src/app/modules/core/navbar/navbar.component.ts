import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';

import { environment as ENV } from '@environment';
import { IS_MOBILE } from '@app/constants/app.tokens';
import { Product } from '@app/modules/products-container/entities/product.entities';
import { ProductsFacade } from '@app/modules/products-container/products-container.facade';
import { Router } from '@angular/router';
import { ROUTES } from '@app/modules/products-container/constants/routes';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  public logo: string = ENV.logo;

  constructor(
    @Inject(IS_MOBILE) public isMobile: boolean,
    private router: Router,
    private productsFacade: ProductsFacade) { }

  ngOnInit(): void { }

  get shoppingCartList$(): Observable<Product[]> {
    return this.productsFacade.shoppingCart$;
  }

  public goToCheckout(): void {
    this.router.navigate([ROUTES.CHECKOUT]);
  }

}
