import { trigger, transition, style, animate } from '@angular/animations';

export const carouselAnimation = trigger('carouselAnimation', [
  transition('void => *', [
    style({ transform: 'translateX(100%)' }),
    animate('300ms ease-in-out', style({ transform: 'translateX(0%)' }))
  ]),
  transition('* => void', [
    style({ transform: 'translateX(0%)' }),
    animate('300ms ease-in-out', style({ transform: 'translateX(100%)' }))
  ])
]);
