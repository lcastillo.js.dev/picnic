export interface GeneralEntity {
  [key: string]: string | number;
}
