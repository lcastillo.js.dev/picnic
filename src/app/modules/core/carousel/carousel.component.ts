import { Component, OnInit, Input, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { interval, Subscription, Observable } from 'rxjs';

import { GeneralEntity } from '../entities/general';
import { Product } from '@app/modules/products-container/entities/product.entities';
import { carouselAnimation } from '../ui/animations/carousel.animation';
import { ROUTES } from '@app/modules/products-container/constants/routes';

const MAX_CAROUSEL_IMGS = 4;
const MAX_ON_BOARDING_IMAGES = 3;
const CAROUSEL_CHANGE_INTERVAL = 5000;

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.sass'],
  animations: [ carouselAnimation ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CarouselComponent implements OnInit, OnDestroy {

  @Input() carouselImages: (Product | {  image: string, name: string })[];
  @Input() autoPlay: boolean = true;
  @Input() onBoarding: boolean = false;
  @Input() width: string;
  @Input() height: string;

  public filled: number = 0;
  public selectedItem: number = 0;
  public interval: Observable<number>;
  public MAX_ON_BOARDING_IMAGES: number = MAX_ON_BOARDING_IMAGES;
  private subscriptions: Subscription[] = [];

  constructor(private router: Router,
              private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    if (this.autoPlay) this.setCarouselTimer();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  get images(): GeneralEntity[] {
    if (!this.carouselImages) return;
    const images = this.carouselImages.map(({ image, name }, index) => ({ index, image, name }));
    return this.onBoarding ? images : images.slice(0, MAX_CAROUSEL_IMGS);
  }

  get imagesIndexes(): (number | string)[] {
    return this.images.map(({ index }) => index);
  }

  public changeSlide(): void {
    if (this.selectedItem === MAX_ON_BOARDING_IMAGES - 1) this.router.navigate([ROUTES.LIST]);
    else this.selectedItem ++;
  }

  private setCarouselTimer(): void {
    if (!this.carouselImages && !this.images) return;
    this.interval = interval(CAROUSEL_CHANGE_INTERVAL);
    this.subscriptions.push(
      this.interval.subscribe(() =>
       (this.selectedItem === (MAX_CAROUSEL_IMGS - 1) ? (this.selectedItem = 0) : (this.selectedItem += 1), this.cdr.detectChanges()),
      )
    )
  }

}
