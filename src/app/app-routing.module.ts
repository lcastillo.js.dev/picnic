import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '', loadChildren: () => 
      import('./modules/home/home.module').then(m => m.HomeModule)
  },
  {
    path: '', loadChildren: () => 
      import('./modules/products-container/products-container.module').then(m => m.ProductsContainerModule)
  },
  {
    path: '', loadChildren: () => 
      import('./modules/shopping-cart/shopping-cart.module').then(m => m.ShoppingCartModule)
  },
  { path: '**', redirectTo: '/list' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
