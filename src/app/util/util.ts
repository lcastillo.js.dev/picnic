const typeCache: { [label: string]: boolean } = {};

export function type<T>(label: T | ''): T {
  if (typeCache[<string>label]) {
    throw new Error(`Action type "${label}" is not unique"`);
  }
  typeCache[<string>label] = true;
  return <T>label;
}

function testRegex(regex: RegExp): boolean {
  return regex.test(navigator.userAgent.toLowerCase());
}

export function isMobile(): boolean {
  const isiPad = testRegex(/ipad/i);
  const isiPod = testRegex(/ipod/i);
  const isWebOS = testRegex(/webos/i);
  const isiPhone = testRegex(/iphone/i);
  const isAndroid = testRegex(/android/i);
  const isiDevice = testRegex(/ipad|iphone|ipod/i);
  const isWindowsPhone = testRegex(/windows phone/i);
  const isSamsungBrowser = testRegex(/SamsungBrowser/i);
  const isMobileSafariBrowser = testRegex(/mobile safari/i);

  return isAndroid || isiPad || isiPhone || isiPod || isiDevice
    || isWebOS || isWindowsPhone || isSamsungBrowser || isMobileSafariBrowser;
}
