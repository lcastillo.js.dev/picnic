import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations"

import { AppRoutingModule } from './app-routing.module';
import { AppContainer } from './app.container';
import { NavbarModule } from './modules/core/navbar/navbar.module';
import { HttpClientModule } from '@angular/common/http';
import { IS_MOBILE } from './constants/app.tokens';
import { isMobile } from './util/util';
import { SentryErrorHandler } from './sentry-error-handler.service';

@NgModule({
  declarations: [
    AppContainer,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    StoreModule.forRoot([]),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      maxAge: 50
    }),
    NavbarModule
  ],
  providers: [
    {
      provide: IS_MOBILE, useFactory: isMobile
    },
    { provide: ErrorHandler, useClass: SentryErrorHandler }
  ],
  bootstrap: [AppContainer]
})
export class AppModule { }
