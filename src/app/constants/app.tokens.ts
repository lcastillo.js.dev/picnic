import { InjectionToken } from '@angular/core';

export const IS_MOBILE = new InjectionToken('isMobile');
