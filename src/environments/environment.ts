// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  SENTRY: 'https://81fa78c879644f2f8c7c927572f5c530@o397267.ingest.sentry.io/5251598',
  logo: 'https://startupjoblist.com/wp-content/uploads/2020/04/picnicLOGO-1024x1024.png',
  api: {
    cors: 'https://cors-anywhere.herokuapp.com/',
    baseUrl: 'https://s3-eu-west-1.amazonaws.com/developer-application-test'
  }
};
